const ingressPrimeStatsToJson = require("ingress-prime-stats-to-json")
const CustomError = require("./customErrorHandler")

class IngressStatsHelper {
    constructor(data) {
        this.data = data
    }
    async proceed() {
        let result;
        try {
            result = await ingressPrimeStatsToJson(this.data)
        } catch (error) {
            throw new CustomError('WRONG_STATS_FORMAT', 'Format stats Ingress Salah')
        }
        if (result['Time Span'] != 'ALL TIME')
            throw new CustomError('WRONG_STATS_TIME', 'Submit stats must from ALL TIME')
        this.data = Object.keys(result)
        this.data.push('\n')
        this.data.forEach(key => {
            this.data.push(result[key])
        });
        this.formated = this.data.join('        ').split('\n        ').join('\n')
        this.json = result
        delete this.data
    }
    // async list()
}

module.exports = IngressStatsHelper