const CustomError = require("./customErrorHandler")

function getArgs(msg) {
    const splitMsg = msg.split(' ')
    if (!splitMsg[1])
        throw new CustomError('ER_ARGS_NO_COMMAND')
    if (splitMsg.length > 2)
        throw new CustomError('ER_ARGS_FALSE_FORMAT')
    return splitMsg.slice(1, splitMsg.length).join(' ')
}

module.exports = getArgs