const bot = require('../connections/botConnection')
const extra = require('telegraf/extra')
const ScoringController = require("../controllers/scoringController");

async function endStatNotification() {
    const user = new ScoringController()
    await user.notificationEndStat()
    if (user.data.length) {
        const msg = `Hayo lo... 😏\nJangan lupa untuk submit stat akhir\n${user.data}\n😉`
        bot.telegram.sendMessage(process.env.IFS_GROUP_ID, msg, extra.markdown())
    }
}

module.exports = endStatNotification