const CustomError = require("../helpers/customErrorHandler")

module.exports = function (ctx, next) {
    if (String(ctx.update.message.from.id) != process.env.MASTER_ID)
        throw new CustomError('UNREPLY_ERROR')
    next()
}