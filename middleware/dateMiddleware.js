const CustomError = require("../helpers/customErrorHandler")
const axios = require('axios')

module.exports = async function (ctx, next) {
    const comm = ctx.update.message.text.split(' ')[0]
    const data = {}

    if (process.env.MT_STATS != 'on') {
        await axios({
            url: process.env.GSCRIPT_URL + '?script=getDateConf',
            method: 'post',
            data: { comm: 'event' }
        })
            .then(res => {
                data.dateConf = res.data.response.result
            })

        let dateMin = new Date(data.dateConf.minDate)
        let dateMax = new Date(data.dateConf.maxDate)
        const date = new Date()

        if (date.getTime() < dateMin.getTime())
            throw new CustomError('UPCOMING_EVENT', `${comm} tidak bisa digunakan, IFS akan diselenggarakan tanggal ${dateMin.toLocaleDateString('en-GB')}`)
        if (date.getTime() > dateMax.getTime())
            throw new CustomError('MISSED EVENT', `${comm} tidak bisa digunakan, IFS terakhir telah lewat dari jadwal`)

        await axios({
            url: process.env.GSCRIPT_URL + '?script=getDateConf',
            method: 'post',
            data: { comm }
        })
            .then(res => {
                dateMin = new Date(res.data.response.result.minDate)
                dateMax = new Date(res.data.response.result.maxDate)
            })

        if (date.getTime() < dateMin.getTime())
            throw new CustomError('UPCOMING_EVENT', `${comm} tidak bisa digunakan sebelum ${dateMin.toLocaleString('en-GB')}`)
        if (date.getTime() > dateMax.getTime())
            throw new CustomError('MISSED_EVENT', `${comm} tidak bisa digunakan setelah ${dateMax.toLocaleString('en-GB')}`)
    }
    next()

}