const userCaller = require("../helpers/callUserHelpers")

const errorIdentified = {
    "ER_DUP_ENTRY": "Command has been added before",
    "ER_ARGS_NO_COMMAND": `Please give add argument after command`,
    "ER_ARGS_FALSE_FORMAT": `Argument format isn't right`
}

async function errorHandler(ctx, err) {
    let msg = err.message
    if (err.name == 'UNREPLY_ERROR')
        return
    if (Object.keys(errorIdentified).includes(err.name))
        msg = errorIdentified[err.name]
    const user = ctx.update.message.from
    ctx.replyWithMarkdown(`${userCaller(user)}\n⛔️ ${msg} ⛔️`)
    return
}

module.exports = errorHandler