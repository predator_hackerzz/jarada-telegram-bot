require('dotenv').config()
const bot = require('./connections/botConnection')
const path = require('path')
const readdirp = require('readdirp')

//** Maintenance status is off */
process.env.MT_STAT = 'off'

//** Load path of the route and sorting it */
readdirp('.', { fileFilter: '*Router.js', directoryFilter: ['!limited'] })
    .on('data', (route) => {
        bot.use(require(`./${route.path}`))
        console.log(`Route loader:\t${path.basename(route.path)}`);
    })
readdirp('.', { fileFilter: '*Router.js', directoryFilter: ['!public'] })
    .on('data', (route) => {
        bot.use(require(`./${route.path}`))
        console.log(`Route loader:\t${path.basename(route.path)}`);
    })

bot.launch()
