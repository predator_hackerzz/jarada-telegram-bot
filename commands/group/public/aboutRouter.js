const Router = require('telegraf/router')
const userCaller = require('../../../helpers/callUserHelpers')

const routing = new Router(({ message }) => {
    try { return { route: message.text } } catch (error) { }
})

routing.on('/about', async (ctx) => {
    const msg = '👋️ Halo Om/Tante' +
        '\n' +
        'Namaku adalah Jarada 🤖️, kata creator aku namaku merupakan akronim dari Jarvis dan ADA' +
        '\n' +
        'Maaf ya Om/Tante kalau aku masih cuma bisa bantu sedikit di grub IFS JATIM' +
        '\n\n' +
        'Salam kenal dari aku dan creator aku 👋️👋️👋️' +
        '\n' +
        'SkyJackerz 👨‍💻️'

    const response = await ctx.getChat()
    if (response.type != 'private') {
        const user = ctx.update.message.from
        ctx.replyWithMarkdown(`${userCaller(user)}\n${msg}`)
    }
})

module.exports = routing