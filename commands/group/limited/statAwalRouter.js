const Router = require('telegraf/router')
const Composer = require('telegraf/composer')
const userCaller = require('../../../helpers/callUserHelpers')
const dateMiddleware = require("../../../middleware/dateMiddleware");
const errorHandler = require("../../../middleware/errorHandler");
const ScoringController = require('../../../controllers/scoringController');

const bot = new Composer()

const routing = new Router(({ message }) => {
    try {
        const command = message.text.split(' ')[0]
        if (command == '/statawal')
            return { route: command }
    } catch (error) { }
})

routing.on('/statawal', async (ctx, next) => {
    try {
        await dateMiddleware(ctx, next)

        const { type } = await ctx.getChat()
        if (type != 'private') {
            const scoring = new ScoringController({ userId: ctx.update.message.from.id, body: ctx.update.message.text })
            await scoring.submitStat()
            const msg = `${scoring.data.join('\n')}\nStat awal berhasil disimpan`
            ctx.deleteMessage(ctx.update.message.message_id)
            const user = ctx.update.message.from
            ctx.replyWithMarkdown(`${userCaller(user)}\n${msg}`)
        }
    } catch (err) {
        bot.use(errorHandler(ctx, err))
        ctx.deleteMessage(ctx.update.message.message_id)
    }
})

module.exports = routing