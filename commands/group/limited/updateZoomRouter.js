const Router = require('telegraf/router')
const Composer = require('telegraf/composer')
const telegram = require('../../../connections/botConnection').telegram
const errorHandler = require("../../../middleware/errorHandler");
const ZoomController = require('../../../controllers/zoomController');
const adminMiddleware = require('../../../middleware/adminMiddleware');

const bot = new Composer();

const routing = new Router(({ message }) => {
    try {
        const command = message.text.split(' ')[0]
        if (command == '/updatezoom')
            return { route: command }
    } catch (error) { }
})

routing.on('/updatezoom', async (ctx, next) => {
    try {
        await adminMiddleware(ctx, next)

        const response = await ctx.getChat()
        if (response.type != 'private') {
            const zoom = new ZoomController(ctx.update.message.text)
            ctx.deleteMessage(ctx.update.message.message_id)
            await zoom.update()
            telegram.sendMessage(process.env.MASTER_ID, `Zoom link has updated to ${zoom.data}`)
        }
    } catch (error) {
        ctx.deleteMessage(ctx.update.message.message_id)
            .catch(err => { })
        bot.use(errorHandler(ctx, error))
    }
})

module.exports = routing