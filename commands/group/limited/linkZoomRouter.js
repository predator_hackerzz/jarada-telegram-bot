const { Extra } = require('telegraf')
const Router = require('telegraf/router')
const Composer = require('telegraf/composer')
const userCaller = require('../../../helpers/callUserHelpers')
const errorHandler = require("../../../middleware/errorHandler");
const ZoomController = require('../../../controllers/zoomController');
const dateMiddleware = require('../../../middleware/dateMiddleware');

const bot = new Composer();

const routing = new Router(({ message }) => {
    try { return { route: message.text } } catch (error) { }
})

routing.on('/linkzoom', async (ctx, next) => {
    try {
        await dateMiddleware(ctx, next)

        const zoom = new ZoomController()
        await zoom.get()

        const msg = 'Ini link zoomnya:\n' +
            `👉️ [KLIK DI SINI](${zoom.data})` +
            '\n\n' +
            '⚠️ Jangan lupa ya untuk rename ketika di dalam zoom\n' +
            '👉️ ID Ingress - [RES/ENL]'

        const response = await ctx.getChat()
        if (response.type != 'private') {
            const user = ctx.update.message.from
            ctx.replyWithMarkdown(`${userCaller(user)}\n${msg}`, Extra.webPreview(false))
        }
    } catch (error) {
        bot.use(errorHandler(ctx, error))
    }
})

module.exports = routing