const axios = require('axios');
const ingressPrimeStatsToJson = require('ingress-prime-stats-to-json');
const userCaller = require('../helpers/callUserHelpers');
const CustomError = require("../helpers/customErrorHandler");
const IngressStatsHelper = require('../helpers/ingressStatsHelpers');

class ScoringController {
    constructor(data) {
        this.data = data
    }
    async submitStat() {
        let { userId, body } = this.data
        if (body.split(' ').length == 1)
            throw new CustomError('MISSING_STATS', 'Format submit stat awal seharusnya: /statawal<spasi>STATS INGRESS PRIME')
        const stats = new IngressStatsHelper(body.replace('/statawal ', ''))
        await stats.proceed()
        await axios({
            url: process.env.GSCRIPT_URL + '?script=submitStats',
            method: 'post',
            data: {
                userId,
                stats: stats.formated
            }
        })
            .then(res => {
                if (res.data.response.code != 200)
                    throw new CustomError("ERR_SUBMIT_STATS", `Maaf ada error: ${res.data.response.result}`)
                this.data = []
            })
        Object.keys(stats.json).forEach(element => {
            if (['Agent Name', 'Level', 'Lifetime AP', 'XM Recharged'].includes(element))
                this.data.push(`${element}:\t${stats.json[element]}`)
        });
    }
    async privateSubmitStat() {
        let { userId, body } = this.data
        if (body.split(' ').length == 1)
            throw new CustomError('MISSING_STATS', 'Format submit stat awal seharusnya: /privatestatawal<spasi>STATS INGRESS PRIME')
        const stats = new IngressStatsHelper(body.replace('/privatestatawal ', ''))
        await stats.proceed()
        await axios({
            url: process.env.GSCRIPT_URL + '?script=submitStats',
            method: 'post',
            data: {
                userId,
                stats: stats.formated
            }
        })
            .then(res => {
                if (res.data.response.code != 200)
                    throw new CustomError("ERR_SUBMIT_STATS", `Maaf ada error: ${res.data.response.result}`)
                this.data = []
            })
        Object.keys(stats.json).forEach(element => {
            if (['Agent Name', 'Level', 'Lifetime AP', 'XM Recharged'].includes(element))
                this.data.push(`${element}:\t${stats.json[element]}`)
        });
    }
    async updateStat() {
        let { userId, body } = this.data
        if (body.split(' ').length == 1)
            throw new CustomError('MISSING_STATS', 'Format submit stat akhir seharusnya: /statakhir<spasi>STATS INGRESS PRIME')
        const stats = new IngressStatsHelper(body.replace('/statakhir ', ''))
        await stats.proceed()
        await axios({
            url: process.env.GSCRIPT_URL + '?script=updateStats',
            method: 'post',
            data: {
                userId,
                stats: stats.formated
            }
        })
            .then(res => {
                if (res.data.response.code != 200)
                    throw new CustomError("ERR_SUBMIT_STATS", `Maaf ada error: ${res.data.response.result}`)
                this.result = res.data.response.result
                this.data = []
            })
        Object.keys(stats.json).forEach(element => {
            if (element == 'Agent Name')
                this.data.push(`${element}:\t${stats.json[element]}`)
            if (['Level', 'Lifetime AP', 'XM Recharged'].includes(element))
                this.data.push(`${element}:\t${stats.json[element]} (+ ${this.result[element]})`)
        });
    }
    async notificationEndStat() {
        await axios({
            url: process.env.GSCRIPT_URL + '?script=getEmptyEndStats',
            method: 'get'
        })
            .then(res => {
                this.data = res.data.response.result
            })
        const listUser = this.data.map(element => userCaller(element))
        this.data = listUser.join(', ')
    }
}

module.exports = ScoringController