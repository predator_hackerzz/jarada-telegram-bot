const axios = require('axios')
const CustomError = require('../helpers/customErrorHandler')

class ZoomController {
    constructor(data) {
        this.data = data
    }
    async get() {
        await axios({
            url: process.env.GSCRIPT_URL + '?script=getZoomLink',
            method: 'get'
        })
            .then(res => {
                this.data = res.data.response.result
            })
    }
    async update() {
        const link = this.data.replace('/updatezoom ', '')
        if (link == '/updatezoom')
            throw new CustomError('UPDATE_ZOOM_LINK_EMPTY', 'Tolong masukkan link zoom ketika update')
        await axios({
            url: process.env.GSCRIPT_URL + '?script=updateZoomLink',
            method: 'post',
            data: { link }
        })
            .then(res => {
                if (res.data.response.code != 200)
                    throw new CustomError('UPDATE_ZOOM_LINK_ERROR', 'Something went wrong went update zoom link')
            })
        this.data = link
    }
}

module.exports = ZoomController