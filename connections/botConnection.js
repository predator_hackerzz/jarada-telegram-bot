const { Telegraf } = require('telegraf');
const TelegrafLogger = require('telegraf-logger');

const bot = new Telegraf(process.env.BOT_TOKEN)

// const logger = new TelegrafLogger({
//     log: console.log,
//     format: '%ut => @%u %fn %ln (%fi): <%ust> %c',
//     contentLength: 100,
// });

// bot.use(logger.middleware());

module.exports = bot